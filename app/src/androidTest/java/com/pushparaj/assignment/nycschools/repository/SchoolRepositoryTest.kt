package com.pushparaj.assignment.nycschools.repository

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.pushparaj.assignment.nycschools.db.DAOAccess
import com.pushparaj.assignment.nycschools.db.SchoolDatabase
import com.pushparaj.assignment.nycschools.model.School
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class SchoolRepositoryTest {
    lateinit var schoolDao: DAOAccess
    lateinit var context: Context
    @Mock
    lateinit var repository: SchoolRepository

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this);
        context = ApplicationProvider.getApplicationContext()
        schoolDao = SchoolDatabase.getDatabase(context).getDAOAccess()
        val schoolInfoA = School("123", "A Texas University", "A Texas University", "7344 Parkridge Blvd", "xxx@gmail.com", "www.atexas.com", "425-445-5565")
        val schoolInfoB = School("124", "B Texas University", "B Texas University", "7344 Parkridge Blvd", "yyy@gmail.com", "www.atexas.com", "425-445-5565")
        val schoolInfoC = School("125", "C Texas University", "C Texas University", "7344 Parkridge Blvd", "zzz@gmail.com", "www.atexas.com", "425-445-5565")
        schoolDao.insertAllSchools(arrayListOf(schoolInfoA, schoolInfoB, schoolInfoC))
    }

    @Test
    fun testInsert() {
        val school1 = schoolDao.getSchool("123")

        Assert.assertTrue(school1 != null)

        val school2 = schoolDao.getSchool("124")
        Assert.assertTrue(school2 != null)

        val school3 = schoolDao.getSchool("125")
        Assert.assertTrue(school3 != null)
    }

    @After
    fun clearDB() {
        GlobalScope.launch {
            schoolDao.clearSchools()
        }

    }
}