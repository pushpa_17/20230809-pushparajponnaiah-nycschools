package com.pushparaj.assignment.nycschools.repository

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.pushparaj.assignment.nycschools.db.DAOAccess
import com.pushparaj.assignment.nycschools.db.SchoolDatabase
import com.pushparaj.assignment.nycschools.model.SchoolDetails
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.MockitoAnnotations

@RunWith(AndroidJUnit4::class)
class SchoolDetailsRepositoryTest {
    lateinit var schoolDao: DAOAccess
    lateinit var context: Context
    @Mock
    lateinit var repository: SchoolDetailsRepository

    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this);
        context = ApplicationProvider.getApplicationContext()
        schoolDao = SchoolDatabase.getDatabase(context).getDAOAccess()
        val schoolInfoA = SchoolDetails("123", "A Texas University", "299", "400", "500", "40")
        val schoolInfoB = SchoolDetails("124", "B Texas University", "399", "500", "600", "50")
        val schoolInfoC = SchoolDetails("125", "C Texas University", "499", "600", "700", "60")
        schoolDao.insertAllSchoolDetails(arrayListOf(schoolInfoA, schoolInfoB, schoolInfoC))
    }

    @Test
    fun testInsert() {
        val school1 = schoolDao.getSchoolDetails("123")

        Assert.assertTrue(school1 != null)

        val school2 = schoolDao.getSchoolDetails("124")
        Assert.assertTrue(school2 != null)

        val school3 = schoolDao.getSchoolDetails("125")
        Assert.assertTrue(school3 != null)
    }

    @After
    fun clearDB() {
        GlobalScope.launch {
            schoolDao.clearSchools()
        }

    }
}