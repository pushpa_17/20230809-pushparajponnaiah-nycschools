package com.pushparaj.assignment.nycschools.ui

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.test.core.app.ApplicationProvider
import com.pushparaj.assignment.nycschools.db.DAOAccess
import com.pushparaj.assignment.nycschools.db.SchoolDatabase
import com.pushparaj.assignment.nycschools.repository.SchoolListState
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import java.util.concurrent.CountDownLatch

@RunWith(MockitoJUnitRunner::class)
class SchoolListViewModelTest {
    lateinit var schoolDao: DAOAccess
    lateinit var context: Context

    lateinit var viewModel: SchoolListViewModel
    lateinit var networkResult: SchoolListState

    // To sync the asycn task
    var countDownLatch = CountDownLatch(1)
    var observer: Observer<SchoolListState> =
        Observer<SchoolListState> { schoolResource ->
            countDownLatch.countDown()
            networkResult = schoolResource
        }

    @Rule
    var task = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        context = ApplicationProvider.getApplicationContext()
        schoolDao = SchoolDatabase.getDatabase(context).getDAOAccess()
        viewModel = Mockito.mock(SchoolListViewModel::class.java)
    }

    @Test
    fun makeServiceCall() {
        viewModel.schools.observeForever(observer)
        viewModel.fetchSchoolsInfo()
        try {
            // wait till the thread finish the task
            countDownLatch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        // observer would update this result
        Assert.assertTrue(!schoolDao.getAllSchools().isEmpty())
        Assert.assertTrue(!schoolDao.getAllSchoolDetails().isEmpty())
    }
}