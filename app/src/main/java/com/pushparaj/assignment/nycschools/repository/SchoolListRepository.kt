package com.pushparaj.assignment.nycschools.repository

import android.content.Context
import android.util.Log
import com.pushparaj.assignment.nycschools.api.ApiService
import com.pushparaj.assignment.nycschools.db.DAOAccess
import com.pushparaj.assignment.nycschools.model.School
import com.pushparaj.assignment.nycschools.utils.NetworkUtils
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

open class SchoolListRepository @Inject constructor(@ApplicationContext appContext: Context, private val daoAccess:DAOAccess, private val apiService: ApiService) {

    private val context = appContext;

    suspend fun getSchools(): SchoolListState? {
        return try {
            val schools: List<School> = daoAccess.getAllSchools()
            if(schools.isNotEmpty()) {
                return SchoolListStateData(schools)
            }
            if(!NetworkUtils.isNetworkConnected(context)) {
                return SchoolListStateError("An error occurred")
            }
            val response = apiService.getSchools().execute()
            if (response.isSuccessful) {
                response.body()?.let {
                    withContext(Dispatchers.IO) {
                        insert(it)
                    }
                    SchoolListStateData(it)
                }
            } else {
                Log.d("SchoolDetailsRepository", "Error: ${response.errorBody().toString()}")
                SchoolListStateError(response.errorBody().toString())
            }
        } catch (e: Exception) {
            Log.d("SchoolDetailsRepository", "Error: ${e.localizedMessage}")
            SchoolListStateError(e.localizedMessage)
        }
    }

    suspend fun insert(schools: List<School>) {
        daoAccess?.insertAllSchools(schools)
    }
}
