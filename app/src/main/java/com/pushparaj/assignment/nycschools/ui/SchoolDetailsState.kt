package com.pushparaj.assignment.nycschools.repository

import com.pushparaj.assignment.nycschools.model.SchoolDetails

/**
 * Class maintains the state of school details
 */
sealed class SchoolDetailsState
object SchoolDetailsStateLoading : SchoolDetailsState()
data class SchoolDetailsStateData(val schoolDetails: SchoolDetails) : SchoolDetailsState()
data class SchoolDetailsStateError(val error: String) : SchoolDetailsState()

