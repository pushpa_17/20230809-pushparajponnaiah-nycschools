package com.pushparaj.assignment.nycschools.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pushparaj.assignment.nycschools.repository.SchoolDetailsRepository
import com.pushparaj.assignment.nycschools.repository.SchoolListState
import com.pushparaj.assignment.nycschools.repository.SchoolListRepository
import com.pushparaj.assignment.nycschools.repository.SchoolListStateLoading
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor (
    private val schoolListRepository: SchoolListRepository,
    private val schoolDetailsRepository: SchoolDetailsRepository): ViewModel() {
    // For School List
    private val _schools = MutableLiveData<SchoolListState>()
    val schools: LiveData<SchoolListState>
        get() = _schools

    init {
        fetchSchoolsInfo()
    }

    /**
     * Fetch all school info
     */
    fun fetchSchoolsInfo() = viewModelScope.launch(Dispatchers.IO) {
        _schools.postValue(SchoolListStateLoading)
        launch {
            schoolListRepository.getSchools()?.let {
                _schools.postValue(it)
            }
            schoolDetailsRepository.fetchAllSchoolDetails()
        }
    }
}

