package com.pushparaj.assignment.nycschools.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pushparaj.assignment.nycschools.repository.SchoolDetailsRepository
import com.pushparaj.assignment.nycschools.repository.SchoolDetailsState
import com.pushparaj.assignment.nycschools.repository.SchoolDetailsStateLoading
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailsViewModel @Inject constructor(
    private val schoolDetailsRepository: SchoolDetailsRepository): ViewModel() {

    // For School Details
    private val _schoolDetails = MutableLiveData<SchoolDetailsState>()
    val schoolDetails: LiveData<SchoolDetailsState>
        get() = _schoolDetails

    fun fetchSchoolDetails(dbn: String) = viewModelScope.launch(Dispatchers.IO) {
        _schoolDetails.postValue(SchoolDetailsStateLoading)
        val job = launch {
                schoolDetailsRepository.getSchoolDetails(dbn)?.let {
                    _schoolDetails.postValue(it)
                }
            }
        }


}