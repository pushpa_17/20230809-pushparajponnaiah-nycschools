package com.pushparaj.assignment.nycschools.repository

import android.content.Context
import android.util.Log
import com.pushparaj.assignment.nycschools.api.ApiService
import com.pushparaj.assignment.nycschools.db.DAOAccess
import com.pushparaj.assignment.nycschools.model.SchoolDetails
import com.pushparaj.assignment.nycschools.utils.NetworkUtils
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

open class SchoolDetailsRepository @Inject constructor(@ApplicationContext appContext: Context, private val daoAccess:DAOAccess, private val apiService: ApiService) {
    private val context = appContext;

    /**
     * Get All school details
     */
    suspend fun fetchAllSchoolDetails() {
        try {
            val schoolDetails: List<SchoolDetails> = daoAccess.getAllSchoolDetails()
            if(schoolDetails.isNotEmpty()) {
                return
            }
            if(NetworkUtils.isNetworkConnected(context)) {
                val response = apiService.getAllSchoolDetails().execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        withContext(Dispatchers.IO) {
                            insert(it)
                        }
                    }
                } else {
                    Log.d("SchoolListRepository", "Error: ${response.errorBody().toString()}")
                }
            } else {
                Log.d("SchoolListRepository", "Network Error: No Network connection")
            }
        } catch (e: Exception) {
            Log.d("SchoolListRepository", "Error: ${e.localizedMessage}")
        }
    }

    /**
     * Get the school detail for the particular school
     */
    suspend fun getSchoolDetails(dbn: String): SchoolDetailsState? {
        return try {
            val schoolDetails: SchoolDetails = daoAccess.getSchoolDetails(dbn)
            if(schoolDetails !=null ) {
                return SchoolDetailsStateData(schoolDetails)
            }
            if(NetworkUtils.isNetworkConnected(context)) {
                val response = apiService.getSchoolDetails(dbn).execute()
                if (response.isSuccessful) {
                    response.body()?.let {
                        withContext(Dispatchers.IO) {
                            if(it.isNotEmpty()) {
                                insert(it.get(0))
                                SchoolDetailsStateData(it.get(0))
                            } else {
                                // If the data is not available in the system it returns the first record
                                SchoolDetailsStateData(daoAccess.getSchoolDetails(1))
                            }
                        }
                    }
                } else {
                    Log.d("SchoolDetailsRepository", "Error: ${response.errorBody().toString()}")
                    SchoolDetailsStateError(response.errorBody().toString())
                }
            } else {
                Log.d("SchoolDetailsRepository", "Network Error: No Network connection")
                SchoolDetailsStateError("An error occurred")
            }
        } catch (e: Exception) {
            Log.d("SchoolDetailsRepository", "Error: ${e.localizedMessage}")
            SchoolDetailsStateError(e.localizedMessage)
        }
    }

    /**
     * Insert all school details in db
     */
    suspend fun insert(schoolDetailsList: List<SchoolDetails>) {
        daoAccess?.insertAllSchoolDetails(schoolDetailsList)
    }

    /**
     * Insert school details in the db
     */
    suspend fun insert(schoolDetail: SchoolDetails) {
        daoAccess.insertSchoolDetails(schoolDetail)
    }
}