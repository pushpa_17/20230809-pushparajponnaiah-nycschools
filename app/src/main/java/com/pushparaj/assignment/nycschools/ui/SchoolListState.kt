package com.pushparaj.assignment.nycschools.repository

import com.pushparaj.assignment.nycschools.model.School

/**
 * Class maintains the state of school list
 */
sealed class SchoolListState
object SchoolListStateLoading : SchoolListState()
data class SchoolListStateData(val schools: List<School>): SchoolListState()
data class SchoolListStateError(val error: String?): SchoolListState()
